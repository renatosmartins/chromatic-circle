import './style.scss'

class Step {
  constructor (circle, pitch) {
    this.$circle = circle.$el
    this.$pitch = pitch.$el
    this.$el = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
  }

  render () {
    const circleRect = this.$circle.getBoundingClientRect()
    const pitchRect = this.$pitch.getBoundingClientRect()
    const relativeCoord = {
      x: (pitchRect.left - circleRect.left + pitchRect.width / 2) * 100 / circleRect.width,
      y: (pitchRect.top - circleRect.top + pitchRect.height / 2) * 100 / circleRect.height
    }
    this.$el.setAttribute('viewBox', '0 0 100 100')
    this.$el.setAttribute('class', 'circle-step')
    const $path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    $path.setAttribute('d', `M50,50 L${relativeCoord.x},${relativeCoord.y}`)
    this.$el.appendChild($path)
  }

  remove () {
    this.$el.remove()
  }
}

export default Step
