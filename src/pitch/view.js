import './style.scss'
import Step from '../step'

const pitchClass = 'circle-pitch'
const pitchEmptyClass = `${pitchClass}--empty`
const pitchActiveClass = `${pitchClass}--active`
const pitchHighlightedClass = `${pitchClass}--highlighted`
const pitchOctaveClass = `${pitchClass}--octave`

class PitchView {
  constructor (circle, model, isHighlighted) {
    this.circle = circle
    this.model = model
    this.isHighlighted = isHighlighted
    this.$el = document.createElement('div')
    this.render()
    this.initEvents()
  }

  render () {
    const classes = [pitchClass]
    if (this.model.empty) {
      classes.push(pitchEmptyClass)
    }
    if (this.isHighlighted) {
      classes.push(pitchHighlightedClass)
    }
    this.$el.classList.add(...classes)
    this.$el.innerText = this.model.index
  }

  initEvents () {
    this.model.on('play', (isOctave) => this.activate(isOctave))
    this.model.on('stop', (isOctave) => this.deactivate(isOctave))
    this.$el.addEventListener('click', () => this.model.play(false, 500))
  }

  activate (isOctave) {
    const classes = [pitchActiveClass]
    if (isOctave) classes.push(pitchOctaveClass)
    this.$el.classList.add(...classes)
    this.renderStep()
  }

  deactivate (isOctave) {
    const classes = [pitchActiveClass]
    if (isOctave) classes.push(pitchOctaveClass)
    this.$el.classList.remove(...classes)
    this.removeStep()
  }

  renderStep () {
    this.step = new Step(this.circle, this)
    this.step.render()
    this.circle.$el.appendChild(this.step.$el)
  }

  removeStep () {
    if (this.step) {
      this.step.remove()
    }
  }
}

export default PitchView
