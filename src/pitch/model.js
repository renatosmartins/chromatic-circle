import Observer from '../core/observer'

class Pitch extends Observer {
  constructor (synth, frequency, index) {
    super()
    this.synth = synth
    this.frequency = frequency
    this.index = index
    this.empty = false
    this.active = false
    this.root = index === 0
    this.playing = []
  }

  getFrequency (isOctave) {
    let frequency = this.frequency
    if (isOctave) frequency = frequency.transpose(12)
    return frequency
  }

  play (isOctave, duration) {
    const frequency = this.getFrequency(isOctave)
    if (this.playing.includes(frequency.toFrequency())) {
      return
    }
    if (this.playing.length === 0) {
      this.emit('play', isOctave)
    }
    this.playing.push(frequency.toFrequency())

    this.synth.triggerAttack(frequency)
    if (duration) {
      setTimeout(() => this.stop(isOctave), duration)
    }
  }

  stop (isOctave) {
    const frequency = this.getFrequency(isOctave)
    this.synth.triggerRelease(frequency)

    this.playing.splice(this.playing.indexOf(frequency.toFrequency()), 1)
    if (this.playing.length === 0) {
      this.emit('stop', isOctave)
    }
  }
}

export default Pitch
