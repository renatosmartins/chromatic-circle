import './style.scss'
import { Pitch, PitchView } from '../pitch'
import { MAJOR } from '../core/scale'
import PolySynth from 'tone/Tone/instrument/PolySynth'

const keys = [
  'Z', 'S', 'X', 'D', 'C', 'V', 'G', 'B', 'H', 'N', 'J', 'M',
  'Q', 2, 'W', 3, 'E', 'R', 5, 'T', 6, 'Y', 7, 'U', 'I'
].map((k) => (typeof k === 'number' ? 'Digit' : 'Key') + k)

class Circle {
  constructor ($el, frequencies, options) {
    this.$el = $el
    this.synth = (new PolySynth()).toMaster()
    this.pitches = []
    this.pitchViews = []
    this.options = options // TODO: when options are updated also update the pitches
    this.scale = MAJOR

    frequencies.forEach((f, i) => {
      const pitch = new Pitch(this.synth, f, i)
      const pitchView = new PitchView(this, pitch, this.scale.includes(i))
      this.pitches.push(pitch)
      this.pitchViews.push(pitchView)
    })

    this.render()
    this.listenToKeys()
  }

  render () {
    this.pitchViews.forEach((p) => this.$el.appendChild(p.$el))
  }

  // FIXME: there is some issue with octave keys not playing simultaneous
  listenToKeys () {
    const keysPressed = []
    document.addEventListener('keydown', (e) => {
      const index = keys.indexOf(e.code)
      if (index !== -1) {
        if (keysPressed.includes(e.code)) {
          return
        }
        const isOctave = index >= this.pitches.length
        this.play(index % this.pitches.length, isOctave)
        keysPressed.push(e.code)
      }
    })

    document.addEventListener('keyup', (e) => {
      const index = keys.indexOf(e.code)
      if (index !== -1) {
        const isOctave = index >= this.pitches.length
        this.stop(index % this.pitches.length, isOctave)
        keysPressed.splice(keysPressed.indexOf(e.code), 1)
      }
    })
  }

  play (index, isOctave) {
    if (index < 0 || index >= this.pitches.length) {
      return
    }
    this.pitches[index].play(isOctave)
  }

  stop (index, isOctave) {
    if (index < 0 || index >= this.pitches.length) {
      return
    }
    this.pitches[index].stop(isOctave)
  }
}

export default Circle
