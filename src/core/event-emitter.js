class EventEmitter {
  on (eventName, handler, context) {
    if (context) handler.bind(context)
    if (this._listeners == null) this._listeners = {}
    if (!(eventName in this._listeners)) this._listeners[eventName] = []
    this._listeners[eventName].push(handler)
  }

  emit (eventName, ...params) {
    if (this._listeners == null) this._listeners = {}
    if (eventName in this._listeners) {
      this._listeners[eventName].forEach(handler => {
        setTimeout(() => handler(...params), 0)
      })
    }
  }
}

export default EventEmitter
