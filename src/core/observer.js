import EventEmitter from './event-emitter'

const protectedProps = ['_listeners']

class Observer {
  constructor (data) {
    if (!data) {
      data = this
    }
    return new Proxy(data, traps)
  }
}
Object.setPrototypeOf(Observer.prototype, EventEmitter.prototype)

const traps = {
  set: (obj, key, value) => {
    const isUpdate = key in obj
    const eventType = isUpdate ? 'update' : 'add'
    obj[key] = value
    if (!protectedProps.includes(key)) {
      if (isUpdate) {
        obj.emit(`${eventType}:${key}`, value, obj)
      }
      obj.emit(eventType, value, obj)
    }
    return true
  }
}

export default Observer
