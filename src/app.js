import 'html-loader!./index.html' // eslint-disable-line
import './style.scss'
import Frequency from 'tone/Tone/type/Frequency'
import Observer from './core/observer'
import Circle from './circle'
import Controls from './controls'

document.addEventListener('DOMContentLoaded', () => {
  const intervals = Array.from(Array(12), (v, i) => i)
  const frequencies = Frequency('G3').harmonize(intervals)

  const options = new Observer({
    showIndexes: true,
    showLines: false
  })
  new Circle(document.querySelector('.circle'), frequencies, options) // eslint-disable-line no-new
  new Controls(document.querySelector('.controls'), options) // eslint-disable-line no-new
})
