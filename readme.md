# Chromatic circle

> The chromatic circle is a geometrical space that shows relationships among the 12 equal-tempered pitch classes making up the familiar chromatic scale on a circle. - _from [Wikipedia](https://en.wikipedia.org/wiki/Chromatic_circle)_

## Install

```sh
npm install
```

## Build

```sh
# build files to `public` folder
npx webpack --mode=production
```

or watching for file changes during develpment:

```sh
npx webpack -w
```
