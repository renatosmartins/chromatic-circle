const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = (env, argv) => {
  return {
    mode: 'development',
    entry: './src/app.js',
    output: {
      filename: 'main.js',
      path: path.resolve(__dirname, 'public')
    },
    module: {
      rules: [{
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          }, {
            loader: 'css-loader'
          }, {
            loader: 'sass-loader',
            options: {
              outputStyle: argv.mode === 'production'
                ? 'compressed'
                : 'expanded'
            }
          }
        ]
      }, {
        test: /\.html$/,
        use: [
          {
            loader: 'file-loader?name=[name].[ext]'
          }, {
            loader: 'extract-loader'
          }, {
            loader: 'html-loader',
            options: {
              minimize: argv.mode === 'production'
            }
          }
        ]
      }]
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'style.css'
      })
    ]
  }
}
